#include "Ball.h"
Ball::Ball() {
}

void Ball::setup() {
	x = ofGetWidth()/2;      // give some random positioning
	y = ofRandom(200, ofGetHeight()-200);

	speedX = ofRandom(-2, 2);           // and random speed and direction
	speedY = ofRandom(-1, 1);

	dim = 30; //starting size

	color.setHsb(ofRandom(200,255), 255, 200); // one way of defining digital color is by addressing its 3 components individually (Red, Green, Blue) in a value from 0-255, in this example we're setting each to a random value
}

void Ball::update() {
	if (x < 0) { //check if we have hit any of the 4 walls
		x = 0;
		speedX *= -1;
	}
	else if (x > ofGetWidth()) {
		x = ofGetWidth();
		speedX *= -1;
	}

	if (y < 0) {
		y = 0;
		speedY *= -1;
	}
	else if (y > ofGetHeight()) {
		y = ofGetHeight();
		speedY *= -1;
	}

	x += speedX; //update our position
	y += speedY;

	if(dim>0) dim -= 0.08; //shrink the size
}

void Ball::draw() {
	ofSetColor(color); //set the color and draw the ball
	ofDrawCircle(x, y, dim);
}
