#pragma once

#include "ofMain.h"
#include "Ball.h"

#define NBALLS 200

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		vector <Ball> balls;

		
};
