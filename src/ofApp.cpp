#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	Ball myBall; //make one ball to get us started
	myBall.setup();
	balls.push_back(myBall);
}

//--------------------------------------------------------------
void ofApp::update(){
	
	if (balls.size() < NBALLS) { //if we need more balls, make them
		Ball myBall;
		myBall.setup();
		balls.push_back(myBall);
	}
	else { //otherwise erase the oldest ball
		balls.erase(balls.begin());
	}
	
	for (auto&& ball : balls) { //update the balls
		ball.update();
	}

}

//--------------------------------------------------------------
void ofApp::draw(){
	for (auto&& ball : balls) {
		ball.draw(); //draw the balls
	}
}

